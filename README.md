```
Requirements
* docker
* docker-compose (v 1.16)

To run clone this repository and run the following command from inside the directory

* docker-compose up

the application will be available on port 8080


Migrations need to be run for the first time a new database volume is created. (Which is usually first time in a host
machine).

Enter into the application container shell

* docker-compose exec application sh

Run the migration command

* ./migrate.sh do

This also creates a user account with email ankitml@gmail.com and password as 'mellonblow'


There is a way to do the migrations  by setting up a temporary environment variable 
during the docker compose command

* migrate=do docker-compose up

But this is not working for now and needs to be fixed. Till then above manual migrations should work. 

```

