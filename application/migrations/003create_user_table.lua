package.path = package.path .. ';../?.lua'
local pgmoon = require('pgmoon')

local function create_user_table()
  local pg = pgmoon.new({ host = "db",  database = "postgres" })
  pg:connect()
  query = [[CREATE TABLE users (
             id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
             email CITEXT NOT NULL UNIQUE,
             password text NOT NULL,
             name varchar(120) NOT NULL
           )]]
  return pg:query(query)
end

print(create_user_table())
