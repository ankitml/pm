package.path = package.path .. ';../?.lua'
local pgmoon = require('pgmoon')

local function create_extensions()
  local pg = pgmoon.new({ host = "db",  database = "postgres" })
  pg:connect()
  query = [[CREATE extension citext]]
  return pg:query(query)
end

print(create_extensions())
