package.path = package.path .. ';../?.lua'
local pgmoon = require('pgmoon')

local function create_key_table()
  local pg = pgmoon.new({ host = "db",  database = "postgres" })
  pg:connect()
  query = [[CREATE TABLE userauthkeys (
             id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
             email CITEXT NOT NULL
           )]]
  return pg:query(query)
end

print(create_key_table())
