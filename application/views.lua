local views = {}
local requests = require('requests')
local token = require('token')
local json = require('JSON')
local models = require('models')
local helpers = require('authhelpers')


local function helloworld()
  cookie_value = helpers.get_cookie("Name")
  print(cookie_value)
  if cookie_value == nil then
    name = "World"
  else
    name = cookie_value
  end
  ngx.say('{"val":"Hello ' .. name .. '"}')
end

local function helloname(a)
  helpers.set_cookie("Name",a, false)
  ngx.say('{"val":"Hello ' .. a .. '! The cookie is set"}')
end

local function search(term)
  url = 'https://lcboapi.com/products?q=' .. term
  headers = {Authorization=token}
  response = requests.get{url = url, headers = headers, timeout=1}
  ngx.say(response.text)
end

local function signup()
  local params, err = ngx.req.get_post_args()
  name = params["name"]
  email = params["email"]
  password = params["password"]
  res, err = models.create_new_user(email, password, name)
  if res == nil then
    ngx.status = 400
    ngx.say('{"message":"Looks like this email is already registered"}')
    ngx.exit(400)
  else
    ngx.say('{"created":"OK"}')
    ngx.exit(200)
  end
end

local function login()
  local params, err = ngx.req.get_post_args()
  -- if already authenticated short circuit this process
  if helpers.validate_auth_cookie() then
    ngx.say('{"created":"OK"}')
  else
    email = params["email"]
    password = params["password"]
    is_valid = models.check_user_password(email, password)
    if is_valid then
      key = models.create_auth_key(email)
      is_cookie_set = helpers.set_auth_cookies(key, email)
      if is_cookie_set then
        ngx.say('{"created":"OK"}')
      else
        ngx.exit(400)
        ngx.say('{"message":"Please enable cookies on your browser"}')
      end
    else
      ngx.exit(403)
      ngx.say('{"message":"Incorrect login information"}')
    end
  end
end

local function logout()
  key = helpers.get_auth_cookie()
  models.invalidate_auth_key(key)
  helpers.remove_auth_cookies()
  ngx.say('{"destroyed":"OK"}')
end

views.helloworld = helloworld
views.helloname = helloname
views.search = search
views.signup = signup
views.login = login
views.logout = logout
return views
