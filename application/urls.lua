local router = require "router"
local r = router.new()
local views = require "views"

r:get("/hello", function()
  views.helloworld()
end)

r:get("/hello/:name", function(params)
  views.helloname(params.name)
end)

r:get("/search/:term", function(params)
  views.search(params.term)
end)

r:post('/signup', function()
  views.signup()
end)

r:post('/login', function()
  views.login()
end)

r:get('/logout', function()
  views.logout()
end)

if r:execute(
  ngx.var.request_method,
  ngx.var.request_uri,
  ngx.req.get_uri_args()
) then
  ngx.status = 200
else
  ngx.status = 404
  ngx.print("404 Not found!")
end

ngx.eof()
