local requests = require('requests')
local BASE_URL = 'http://127.0.0.1:8080/'
local tests = {}
local pgmoon = require('pgmoon')

local function test_helloworld()
    url = BASE_URL .. 'hello/'
    response = requests.get(url)
    assert(response.status_code == 200)
end


local function test_search()
    url = BASE_URL .. '/search/abc'
    response = requests.get{url}
    assert(response.status_code == 200)
    assert(response.json()["pager"]["total_record_count"] == 0)

    url = BASE_URL .. '/search/sab'
    response = requests.get(url)
    assert(response.status_code == 200)
    assert(response.json()["pager"]["total_record_count"] > 0)
end


local function test_signup()
    url = BASE_URL .. '/signup/'
    NAME = 'TESTER'
    EMAIL = 'TEST_EMAIL@TEST.COM'
    PASSWORD = 'TEST$%$'
    test_payload = {name=NAME, email=EMAIL, password=PASSWORD}
    response = requests.post{url=url, data=test_payload}
    assert(response.status_code == 200)

    -- Need better testing strategy, API tests directly talking to database is a terrible idea
    local pg = pgmoon.new({ host = "db",  database = "postgres" })
    pg:connect()
    query = [[SELECT count(id) FROM users WHERE email=]] .. EMAIL
    assert(pg:query(query)[1]["count"] == 1)

    query = [[SELECT name FROM users WHERE email=]] .. EMAIL
    assert(pg:query(query)[1]["name"] == NAME)
end


local function test_login()
    url = BASE_URL .. '/login'
    EMAIL = 'TEST_EMAIL@TEST.COM'
    PASSWORD = 'TEST$%$'
    test_payload = {name=NAME, email=EMAIL, password=PASSWORD}
    response = requests.post{url=url, data=test_payload}
    assert(response.status_code == 200)

    local pg = pgmoon.new({ host = "db",  database = "postgres" })
    pg:connect()
    query = [[SELECT count(id) FROM userauthkeys WHERE email=']] .. EMAIL .. "'"
    assert(pg:query(query)[1]["count"] == 1)

    query = [[DELETE FROM userauthkeys WHERE email=']] .. EMAIL .. "'"
    pg:query(query)
end


test_login()
