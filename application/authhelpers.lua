local authhelpers = {}
local ck = require("resty.cookie")
local models = require("models")

local function set_cookie(k,v,httponly)
  local cookie, err = ck:new()
  if not cookie then
    ngx.log(ngx.ERR, err)
    return false
  end
  if httponly == nil then
      httponly = true
  end
  -- set cookie assuming some metadata
  local ok, err = cookie:set({
    key = k, value = v, path = "/",
    domain = "", httponly = httponly,
    expires = 30,
  })
  if not ok then
    ngx.log(ngx.ERR, err)
    return false
  end
  return true
end

local function get_cookie(k)
  local cookie, err = ck:new()
  if not cookie then
    ngx.log(ngx.ERR, err)
    return
  end
  local field, err = cookie:get(k)
  return field
end

local function get_auth_cookie()
  return get_cookie("AuthKey")
end

local function set_auth_cookies(key, email)
  name = models.get_name_from_email(email)
  cookie1 = set_cookie("AuthKey", key)
  cookie2 = set_cookie("UserName", name, false)
  cookie3 = set_cookie("UserEmail", email)
  return (cookie3 and cookie2) and cookie1
end

local function cookie_not_empty(name)
    value = get_cookie(name)
    if (value == nil or value == "") then
        return false
    else
        return true
    end
end

local function validate_auth_cookie()
  if (cookie_not_empty("email") and cookie_not_empty("key")) then
    return models.validate_auth_key(key, email)
  else
    return false
  end
end

local function remove_auth_cookies()
  set_cookie("AuthKey", "")
  set_cookie("UserName", "", false)
  set_cookie("UserEmail", "")
end

authhelpers.set_cookie = set_cookie
authhelpers.set_auth_cookies = set_auth_cookies
authhelpers.get_cookie = get_cookie
authhelpers.get_auth_cookie = get_auth_cookie
authhelpers.validate_auth_cookie = validate_auth_cookie
authhelpers.remove_auth_cookies = remove_auth_cookies
return authhelpers
