local pgmoon = require('pgmoon')
local models = {}


local function create_new_user(email, password, name)
  local pg = pgmoon.new({ host = "db",  database = "postgres" })
  pg:connect()
  query = [[INSERT INTO users (email, password, name)
            VALUES (]] .. pg:escape_literal(email) .. [[,
            crypt(]] .. pg:escape_literal(password) .. [[,
            gen_salt('bf', 8)),]] .. pg:escape_literal(name) .. [[)]]
  return pg:query(query)
end


local function check_user_password(email, pass)
  local pg = pgmoon.new({ host = "db",  database = "postgres" })
  pg:connect()
  query = [[ SELECT count(id) FROM users
               WHERE email=]] .. pg:escape_literal(email) .. [[
               AND password = crypt(]] .. pg:escape_literal(pass) .. [[, password)]]
  return pg:query(query)[1]["count"] == 1
end


local function create_auth_key(email)
  local pg = pgmoon.new({ host = "db",  database = "postgres" })
  pg:connect()
  query = [[INSERT INTO userauthkeys (email)
            VALUES (]] .. pg:escape_literal(email) .. [[)
            RETURNING id]]
  return pg:query(query)[1]["id"]
end

local function validate_auth_key(key, email)
  local pg = pgmoon.new({ host = "db",  database = "postgres" })
  pg:connect()
  query = [[SELECT count(id) FROM userauthkeys
            WHERE email=]] .. pg:escape_literal(email) .. [[
            AND id=]] .. pg:escape_literal(key)
  return pg:query(query)[1]["count"] == 1
end

function get_name_from_email(email)
  local pg = pgmoon.new({ host = "db",  database = "postgres" })
  pg:connect()
  query = [[SELECT name FROM users
            WHERE email=]] .. pg:escape_literal(email)
  return pg:query(query)[1]["name"]
end

function invalidate_auth_key(key)
  local pg = pgmoon.new({ host = "db",  database = "postgres" })
  pg:connect()
  query = [[DELETE FROM userauthkeys WHERE id=]] .. pg:escape_literal(key)
  pg:query(query)
end

models.create_new_user = create_new_user
models.check_user_password = check_user_password
models.create_auth_key = create_auth_key
models.validate_auth_key = validate_auth_key
models.get_name_from_email = get_name_from_email
models.invalidate_auth_key = invalidate_auth_key
return models
