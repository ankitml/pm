#!/bin/sh
# This will  only run migrations when argument is do
# -- example ---
# ./migrate.sh do
if [[ $1 = 'do' ]]
then
    echo 'Running migrations on database'
    resty migrations/001pgcrypt_enable.lua
    resty migrations/002enable_citext.lua
    resty migrations/003create_user_table.lua
    resty migrations/004create_userkey_table.lua
    resty scripts/001create_admin_user.lua
fi
