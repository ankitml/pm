local requests = require('requests')
local token = require('token')

url = 'https://lcboapi.com/products?q=' .. ngx.var.arg_q
headers = {Authorization=token}
response = requests.get{url = url, headers = headers, timeout=1}
ngx.say(response.text)
ngx.exit(200)
