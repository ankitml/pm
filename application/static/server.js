function httpGET(url, success) {
    var xhr = new XMLHttpRequest();
    if (!('withCredentials' in xhr)) xhr = new XDomainRequest(); // fix IE8/9
    xhr.open('GET', url);
    xhr.onload = success;
    xhr.send();
    return xhr;
}

function ajax_search() {
    var input_element = document.getElementById('search-term');
    url = '/search/' + input_element.value
    // delete existing searches
    var results_container = document.getElementById('results');
    results_container.innerHTML = ''
    // fetch and display new results
    httpGET(url, function(request){
        var response = request.currentTarget.response || request.target.responseText;
        display_records(JSON.parse(response));
})};
