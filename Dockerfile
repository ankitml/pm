# Dockerfile for openresty
# VERSION   0.1.4

FROM ankitml/openrestycompiled-base:postgresclient
MAINTAINER Ankit Mittal <ankitml@gmail.com>

# install dependencies (ideally move them out to another Dockerfile, do they
# get baked in an image and dont need install everytime this container is built
RUN luarocks install lua-requests && \
  luarocks install router && \
  luarocks install json-lua && \
  luarocks install pgmoon && \
  luarocks install lua-resty-cookie
ENV PATH="/usr/local/openresty/bin:${PATH}"
CMD  ./migrate.sh ${MIGRATE}

EXPOSE 8080
CMD /usr/local/openresty/nginx/sbin/nginx -p `pwd` -c nginx.conf
