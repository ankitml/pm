
Summary

Overall, I feel the program does what it is supposed to do but has its own flaws and compromises. 
It certainly could have been done a lot better. Also, I had good learnings while doing this project in lua. 
There were some learnings on docker infra side as well, as well because I had not worked with docker-compose before. 

If I had to go back in time, I would re-evaluate my choice of technologies as lua/openresty ecosystem does not have 
good community ecosystem. It does work, but it is hard to find libraries, their documentation and technical knowhow. 
Also, much of the good practices like documents are not available in english, but only in chinese. Because of this
community hiccups, there were few things which had to be done from scratch which otherwise come from free in other 
language/frameworks. 




Stack Chosen :

     Language : Lua, 
          web server : nginx
               Framework: openresty

               Why? 
               I could have chosen few possible stacks. I am most familiar with python (Django stack) which would not be
               a good choice considering the millions concurrent users. However, there are cases where people have been
               able to scale python to that level, yet it is acceptable to say that there are better choices available
               for this problem. 

               I chose the openresty/nginx stack because nginx is a non blocking server and can handle very large number
               of concurrent connections. Openresty is native to nginx so there is no separation between nginx and the
               application code. It will not only scale to millions of users but also good for thin applications which
               don't have large business logic (like this one). Also, it is a new language and stack for me and there is
               large room for learning while doing this project. 

               Openresty can make http calls and can also talk to databases like Postgres, Cassandra and redis, although
               the landscape of ORM doesn't look mature in Lua. 

               docker run -t -i -p 8080:8080 -v=`pwd`:/hello -w=/hello ubinix5warun/openresty-docker

Day 1: 
- Setting up docker and installing dependencies, building nginx. 
       
- Hello world api
       
- Index static page with very minimal UI, a search input box and a button
       
- a dummy search API returning some json
       
- clicking on button calls a dummy api and outputs the result in console.log dummy search API. 
 

Day 2

- cleaning up dockers and baking much of the compiling into a base image
- this image is now put up on github at https://github.com/ankitml/openresty-docker-base and also on dockerhub for anyone to use
- installed luarocks on this base image as package manager for lua packages
- updated project dockerfile to install lua-requests, openresty doesn't recognize this by default
- this is because it seems there are two lua versions on the machine. 5.2 and 5.1. openresty using old one.
- rebuilt the base image and this repository's image with only lua5.1 as openresty doesn't support newer version of lua. 
- requests module working with openresty /search.lua is able to make http requests and use the resutl

Day3
- Javascript / frontend for clicking on search and showing the results
- adding material ui
- reorganized the single html and extracted js into multiple files

Day4
    - NA 😴 😴 
    
Day 5
    - Looking for easy authentication options in openresty. 
    There can be so many things that can go wrong with poorly designed authentication system. Security loopholes are one
    of the biggest risks these days. I dont want you to think that I am not aware of the issues in it. However, also I
    have to deliver this prototype in a short period of time. Hence there need to be a compromise. 
    One solution which comes to mind is to use an existing standard (oauth) and a social authentication provider, I an
    trying to avoid that too in this project. Having implemented social logins in the past, I can safely say that they
    are one of the most unpleseant part of working in the web industry. It is very hard to make them set up. I would try
    almost every other way before treding that part again. Although, as an idea social logins are a very fine way of
    adding authentication, yet their implementation is so bad that it hurts. 

    * Dont store user data in the cookie, store reference to it. Save user data in a fast performant datastore.
    - reorganized routes and views into proper abstractions
    - understood how to make routes and views for POST urls, made dummy signup and login

Day 6
    - learnt docker compose yaml configurations and implemented it
    - made postgres container and app container talk to each other

Day 7
    - migrations for creating basic table
    - postgres added to docker volume for persistence
    - email column is made case insensitive to avoid duplicate user accounts
    - cookie manipulation from backend
    - signup endpoint
    - login endpoint
    - logout support from frontend

Day 8
    - migration via bootstrap (now working correctly)
    - added few tests



